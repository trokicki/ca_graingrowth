﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CAGrainGrowthLibrary;

namespace CA_GrainGrowthTests
{
	[TestFixture]
    public class CellsNeighbourhoodTests
    {
		private Cell[,] cells = new Cell[5, 5];

		private void clearCellsArray() {
			for (int i = 0; i < cells.GetLength(0); i++) {
				for (int j = 0; j < cells.GetLength(1); j++) {
					cells[i, j] = new Cell();
				}
			}
		}

		[SetUp]
		public void SetUp() {
			clearCellsArray();
		}

		//[Test]
		//public void VonNeumannNeighbourhoodTest() {
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(0));
		//	cells[2, 1].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	clearCellsArray();
		//	cells[1, 2].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	clearCellsArray();
		//	cells[3, 2].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	clearCellsArray();
		//	cells[2, 1].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	clearCellsArray();
		//	cells[2, 3].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	cells[2, 1].GermID = 1;
		//	cells[1, 2].GermID = 2;
		//	cells[3, 2].GermID = 1;
		//	cells[2, 3].GermID = 1;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(1));
		//	cells[3, 2].GermID = 3;
		//	cells[2, 3].GermID = 3;
		//	Assert.That(CellsNeighbourhoodChecker.VonNeumann(cells, 2, 2), Is.EqualTo(3));
		//}

		[TearDown]
		public void TearDown() {

		}
    }
}
