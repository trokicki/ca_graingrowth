﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CAGrainGrowthLibrary {
	public class CellsNeighbourhoodChecker {
		private Random random = new Random();

		public enum NeighbourhoodType { Periodic, Absorbing, Bouncing };

		public NeighbourhoodType CellNeighbourhoodType { get; set; }

		public CellsNeighbourhoodChecker(NeighbourhoodType cellNeighbourhoodType) {
			this.CellNeighbourhoodType = cellNeighbourhoodType;
		}

		public int Moore(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column + 1, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int VonNeumann(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int HexagonalLeft(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column + 1, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int HexagonalRight(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column - 1, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int HexagonalRandom(Cell[,] cells, int row, int column) {
			switch (random.Next(2)) {
				case 0:
					return HexagonalLeft(cells, row, column);
				default:
					return HexagonalRight(cells, row, column);
			}
		}

		public int PentagonalLeft(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int PentagonalRight(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int PentagonalTop(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row , column + 1, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int PentagonalBottom(Cell[,] cells, int row, int column) {
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row , column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column - 1, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column , neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column + 1, neighboursAmount);
			checkCellNeighbourhood(cells, row , column + 1, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int PentagonalRandom(Cell[,] cells, int row, int column) {
			switch (random.Next(4)) {
				case 0:
					return PentagonalLeft(cells, row, column);
				case 1:
					return PentagonalRight(cells, row, column);
				case 2:
					return PentagonalTop(cells, row, column);
				default:
					return PentagonalBottom(cells, row, column);
			}
		}

		private void checkCellNeighbourhood(Cell[,] cells, int row, int column, Dictionary<int, int> neighboursAmount) {
			switch (CellNeighbourhoodType) {
				case NeighbourhoodType.Periodic:
					checkPeriodicCellNeighbourhood(cells, row, column, neighboursAmount);
					break;
				case NeighbourhoodType.Absorbing:
					checkAbsorbingCellNeighbourhood(cells, row, column, neighboursAmount);
					break;
			}
		}

		private void checkPeriodicCellNeighbourhood(Cell[,] cells, int row, int column, Dictionary<int, int> neighboursAmount) {
			if (row < 0)
				row = cells.GetUpperBound(0);
			else if (row > cells.GetUpperBound(0))
				row = 0;
			if (column < 0)
				column = cells.GetUpperBound(1);
			else if (column > cells.GetUpperBound(1))
				column = 0;
			checkAbsorbingCellNeighbourhood(cells, row, column, neighboursAmount);
		}

		private void checkAbsorbingCellNeighbourhood(Cell[,] cells, int row, int column, Dictionary<int, int> neighboursAmount) {
			if (row >= 0 && row <= cells.GetUpperBound(0) && column >= 0 && column <= cells.GetUpperBound(1)) {
				Cell keyCell = cells[row, column];
				if (keyCell.GermID > 0) {
					if (!neighboursAmount.ContainsKey(keyCell.GermID))
						neighboursAmount[keyCell.GermID] = 1;
					else
						neighboursAmount[keyCell.GermID]++;
				}
			}
		}

		private int getKeyForMostCommonNeighbour(Dictionary<int, int> neighboursAmount) {
			return neighboursAmount.Aggregate((left, right) => left.Value > right.Value ? left : right).Key;
		}
	}
}
