﻿namespace CAGrainGrowthLibrary
{
    public class Cell
    {
		public int [] Location { get; set; }
		public int GermID { get; set; }
		public bool IsSealed { get; set; }
		public bool IsBorder { get; set; }
		public bool HasRecrystallized { get; set; }
		public int DislocationDensity { get; set; }
        public bool IsInclusion { get; set; }
        public bool IsMartensite { get; set; }

        public Cell() {
			DislocationDensity = 0;
			HasRecrystallized = false;
		}
    }
}
