﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CAGrainGrowthLibrary {
	public static class EnumerableExtensions {
		private static Random random = new Random();

		public static T RandomElement<T>(this IEnumerable<T> enumerable) {
			int randomIndex = random.Next(enumerable.Count());
			return enumerable.Skip(randomIndex).Take(1).Single();
		}

		public static T RandomElement<T>(this List<T> list) {
			int randomIndex = random.Next(list.Count);
			return list[randomIndex];
		}
	}
}
