﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CAGrainGrowthLibrary {
	public class DRXCellsNeighbourhoodChecker {
		public enum NeighbourhoodType { Periodic, Absorbing, Bouncing };

		public NeighbourhoodType CellNeighbourhoodType { get; set; }

		public DRXCellsNeighbourhoodChecker(NeighbourhoodType cellNeighbourhoodType) {
			this.CellNeighbourhoodType = cellNeighbourhoodType;
		}

		public int Moore(Cell[,] cells, int row, int column) {
			int dislocationDensity = cells[row, column].DislocationDensity;
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			checkCellNeighbourhood(cells, row - 1, column - 1, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row - 1, column + 1, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row, column - 1, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row, column + 1, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column - 1, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column, dislocationDensity, neighboursAmount);
			checkCellNeighbourhood(cells, row + 1, column + 1, dislocationDensity, neighboursAmount);
			if (neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int VonNeumann(Cell[,] cells, int row, int column) {
			int dislocationDensity = cells[row, column].DislocationDensity;
			int neighboursDislocationDensity = 0;
			Dictionary<int, int> neighboursAmount = new Dictionary<int, int>();
			neighboursDislocationDensity+= checkCellNeighbourhood(cells, row - 1, column, dislocationDensity, neighboursAmount);
			neighboursDislocationDensity += checkCellNeighbourhood(cells, row, column - 1, dislocationDensity, neighboursAmount);
			neighboursDislocationDensity += checkCellNeighbourhood(cells, row, column + 1, dislocationDensity, neighboursAmount);
			neighboursDislocationDensity += checkCellNeighbourhood(cells, row + 1, column, dislocationDensity, neighboursAmount);
			if (/*neighboursDislocationDensity > dislocationDensity &&  */neighboursAmount.Count > 0)
				return getKeyForMostCommonNeighbour(neighboursAmount);
			return 0;
		}

		public int HexagonalLeft(Cell[,] cells, int row, int column) {
			throw new NotImplementedException();
		}

		public int HexagonalRight(Cell[,] cells, int row, int column) {
			throw new NotImplementedException();
		}

		public int HexagonalRandom(Cell[,] cells, int row, int column) {
			throw new NotImplementedException();
		}

		public int PentagonalRandom(Cell[,] cells, int row, int column) {
			throw new NotImplementedException();
		}

		private int checkCellNeighbourhood(Cell[,] cells, int row, int column, int dislocationsDensity, Dictionary<int, int> neighboursAmount) {
			switch (CellNeighbourhoodType) {
				case NeighbourhoodType.Periodic:
					return checkPeriodicCellNeighbourhood(cells, row, column, dislocationsDensity, neighboursAmount);
				case NeighbourhoodType.Absorbing:
					return checkAbsorbingCellNeighbourhood(cells, row, column, dislocationsDensity, neighboursAmount);
				default:
					return 0;
			}
		}

		private int checkPeriodicCellNeighbourhood(Cell[,] cells, int row, int column, int dislocationsDensity, Dictionary<int, int> neighboursAmount) {
			if (row < 0)
				row = cells.GetUpperBound(0);
			else if (row > cells.GetUpperBound(0))
				row = 0;
			if (column < 0)
				column = cells.GetUpperBound(1);
			else if (column > cells.GetUpperBound(1))
				column = 0;
			return checkAbsorbingCellNeighbourhood(cells, row, column, dislocationsDensity, neighboursAmount);
		}

		private int checkAbsorbingCellNeighbourhood(Cell[,] cells, int row, int column, int dislocationsDensity, Dictionary<int, int> neighboursAmount) {
			if (row >= 0 && row <= cells.GetUpperBound(0) && column >= 0 && column <= cells.GetUpperBound(1)) {
				Cell cellToBeChecked = cells[row, column];
				if (cellToBeChecked.HasRecrystallized) {
					if (!neighboursAmount.ContainsKey(cellToBeChecked.GermID))
						neighboursAmount[cellToBeChecked.GermID] = 1;
					else
						neighboursAmount[cellToBeChecked.GermID]++;
				}
				return cellToBeChecked.DislocationDensity;
			}
			return 0;
		}

		private int getKeyForMostCommonNeighbour(Dictionary<int, int> neighboursAmount) {
			int maxValue = neighboursAmount.OrderByDescending(pair => pair.Value).First().Value;
			List<int> keys = new List<int>();
			foreach (var pair in neighboursAmount) {
				if (pair.Value == maxValue) {
					keys.Add(pair.Key);
				}
			}
			return keys.RandomElement();
			return neighboursAmount.Aggregate((left, right) => left.Value > right.Value ? left : right).Key;
		}
	}
}
