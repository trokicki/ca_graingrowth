﻿using System;

namespace CAGrainGrowthLibrary {
	public class Germ {
		private static int _count = 0;
		public static int Count {
			get {
				return _count;
			}
			private set {
				_count = value;
			}
		}
		public int ID { get; private set; }
		public System.Drawing.Color Color { get; set; }
		public int CellsAmount { get; set; }

		public Germ() {
			ID = ++Count;
		}

		public override string ToString() {
			return String.Format("{0} {1}", "", ID);
		}

		public static void ResetCounter() {
			Count = 0;
		}
	}
}
