﻿using System;
using System.Drawing;

namespace CAGrainGrowthLibrary {
	public class ColorRandomizer {
		private Random colorRandom;

		public ColorRandomizer() {
			colorRandom = new Random();
		}

		public Color RandomColor() {
			return Color.FromArgb(colorRandom.Next(206) + 50, colorRandom.Next(256), colorRandom.Next(256));
		}
	}
}
