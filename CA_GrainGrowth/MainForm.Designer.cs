﻿namespace CA_GrainGrowth {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.DrawingPanel = new System.Windows.Forms.Panel();
            this.StartGrainGrowthButton = new System.Windows.Forms.Button();
            this.Boundaries = new System.Windows.Forms.GroupBox();
            this.AbsorbingRadio = new System.Windows.Forms.RadioButton();
            this.PeriodicRadio = new System.Windows.Forms.RadioButton();
            this.AlgorithmCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GermsRandomizingCombo = new System.Windows.Forms.ComboBox();
            this.GermsListView = new System.Windows.Forms.ListView();
            this.ResetButton = new System.Windows.Forms.Button();
            this.RevertCellsButton = new System.Windows.Forms.Button();
            this.RandomizeGermsButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.GermsCountLabel = new System.Windows.Forms.Label();
            this.WorkingStatusLabel = new System.Windows.Forms.Label();
            this.OperationTabControl = new System.Windows.Forms.TabControl();
            this.GrainGrowthTabPage = new System.Windows.Forms.TabPage();
            this.InclusionGroupBox = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.InclusionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.InclusionAmountNUD = new System.Windows.Forms.NumericUpDown();
            this.InclusionSizeNUD = new System.Windows.Forms.NumericUpDown();
            this.GermRandomizeSettingsTabControl = new System.Windows.Forms.TabControl();
            this.BlankTab = new System.Windows.Forms.TabPage();
            this.RandomWithRadiusTab = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.RandomGermsPositionMinimumRadiusNUD = new System.Windows.Forms.NumericUpDown();
            this.DRXTabPage = new System.Windows.Forms.TabPage();
            this.StartDRXButton = new System.Windows.Forms.Button();
            this.DualPhaseTabPage = new System.Windows.Forms.TabPage();
            this.RunDualPhaseSecondGrainGrowthButton = new System.Windows.Forms.Button();
            this.SelectingMartensiteGrainCheckBox = new System.Windows.Forms.CheckBox();
            this.DualPhaseKeepOriginalColorCheckBox = new System.Windows.Forms.CheckBox();
            this.Boundaries.SuspendLayout();
            this.OperationTabControl.SuspendLayout();
            this.GrainGrowthTabPage.SuspendLayout();
            this.InclusionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InclusionAmountNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InclusionSizeNUD)).BeginInit();
            this.GermRandomizeSettingsTabControl.SuspendLayout();
            this.RandomWithRadiusTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RandomGermsPositionMinimumRadiusNUD)).BeginInit();
            this.DRXTabPage.SuspendLayout();
            this.DualPhaseTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // DrawingPanel
            // 
            this.DrawingPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.DrawingPanel.Location = new System.Drawing.Point(299, 12);
            this.DrawingPanel.Name = "DrawingPanel";
            this.DrawingPanel.Size = new System.Drawing.Size(570, 570);
            this.DrawingPanel.TabIndex = 0;
            this.DrawingPanel.Click += new System.EventHandler(this.DrawingPanel_Click);
            this.DrawingPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawingPanel_Paint);
            // 
            // StartGrainGrowthButton
            // 
            this.StartGrainGrowthButton.Location = new System.Drawing.Point(155, 468);
            this.StartGrainGrowthButton.Name = "StartGrainGrowthButton";
            this.StartGrainGrowthButton.Size = new System.Drawing.Size(118, 52);
            this.StartGrainGrowthButton.TabIndex = 1;
            this.StartGrainGrowthButton.Text = "Start Grain Growth";
            this.StartGrainGrowthButton.UseVisualStyleBackColor = true;
            this.StartGrainGrowthButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // Boundaries
            // 
            this.Boundaries.Controls.Add(this.AbsorbingRadio);
            this.Boundaries.Controls.Add(this.PeriodicRadio);
            this.Boundaries.Location = new System.Drawing.Point(6, 6);
            this.Boundaries.Name = "Boundaries";
            this.Boundaries.Size = new System.Drawing.Size(267, 72);
            this.Boundaries.TabIndex = 2;
            this.Boundaries.TabStop = false;
            this.Boundaries.Text = "Boundaries";
            // 
            // AbsorbingRadio
            // 
            this.AbsorbingRadio.AutoSize = true;
            this.AbsorbingRadio.Checked = true;
            this.AbsorbingRadio.Location = new System.Drawing.Point(7, 44);
            this.AbsorbingRadio.Name = "AbsorbingRadio";
            this.AbsorbingRadio.Size = new System.Drawing.Size(72, 17);
            this.AbsorbingRadio.TabIndex = 1;
            this.AbsorbingRadio.TabStop = true;
            this.AbsorbingRadio.Text = "Absorbing";
            this.AbsorbingRadio.UseVisualStyleBackColor = true;
            // 
            // PeriodicRadio
            // 
            this.PeriodicRadio.AutoSize = true;
            this.PeriodicRadio.Location = new System.Drawing.Point(7, 20);
            this.PeriodicRadio.Name = "PeriodicRadio";
            this.PeriodicRadio.Size = new System.Drawing.Size(63, 17);
            this.PeriodicRadio.TabIndex = 0;
            this.PeriodicRadio.Text = "Periodic";
            this.PeriodicRadio.UseVisualStyleBackColor = true;
            // 
            // AlgorithmCombo
            // 
            this.AlgorithmCombo.FormattingEnabled = true;
            this.AlgorithmCombo.Items.AddRange(new object[] {
            "Von Neumann",
            "Moore",
            "Pentagonal left",
            "Pentagonal right",
            "Pentagonal top",
            "Pentagonal bottom",
            "Pentagonal random",
            "Hexagonal left",
            "Hexagonal right",
            "Hexagonal random"});
            this.AlgorithmCombo.Location = new System.Drawing.Point(97, 90);
            this.AlgorithmCombo.Name = "AlgorithmCombo";
            this.AlgorithmCombo.Size = new System.Drawing.Size(177, 21);
            this.AlgorithmCombo.TabIndex = 3;
            this.AlgorithmCombo.SelectedIndexChanged += new System.EventHandler(this.AlgorithmCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Algorithm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Germs positioning";
            // 
            // GermsRandomizingCombo
            // 
            this.GermsRandomizingCombo.FormattingEnabled = true;
            this.GermsRandomizingCombo.Items.AddRange(new object[] {
            "Click",
            "Random",
            "Regular",
            "Random considering radius"});
            this.GermsRandomizingCombo.Location = new System.Drawing.Point(97, 115);
            this.GermsRandomizingCombo.Name = "GermsRandomizingCombo";
            this.GermsRandomizingCombo.Size = new System.Drawing.Size(134, 21);
            this.GermsRandomizingCombo.TabIndex = 6;
            this.GermsRandomizingCombo.SelectedIndexChanged += new System.EventHandler(this.GermsRandomizingCombo_SelectedIndexChanged);
            // 
            // GermsListView
            // 
            this.GermsListView.BackColor = System.Drawing.SystemColors.Control;
            this.GermsListView.Location = new System.Drawing.Point(6, 385);
            this.GermsListView.Name = "GermsListView";
            this.GermsListView.Size = new System.Drawing.Size(267, 77);
            this.GermsListView.TabIndex = 11;
            this.GermsListView.TileSize = new System.Drawing.Size(100, 15);
            this.GermsListView.UseCompatibleStateImageBehavior = false;
            this.GermsListView.View = System.Windows.Forms.View.Tile;
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(6, 496);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(70, 23);
            this.ResetButton.TabIndex = 12;
            this.ResetButton.Text = "Clear";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // RevertCellsButton
            // 
            this.RevertCellsButton.Location = new System.Drawing.Point(6, 468);
            this.RevertCellsButton.Name = "RevertCellsButton";
            this.RevertCellsButton.Size = new System.Drawing.Size(70, 23);
            this.RevertCellsButton.TabIndex = 13;
            this.RevertCellsButton.Text = "Revert";
            this.RevertCellsButton.UseVisualStyleBackColor = true;
            this.RevertCellsButton.Click += new System.EventHandler(this.RevertCellsButton_Click);
            // 
            // RandomizeGermsButton
            // 
            this.RandomizeGermsButton.Location = new System.Drawing.Point(237, 115);
            this.RandomizeGermsButton.Name = "RandomizeGermsButton";
            this.RandomizeGermsButton.Size = new System.Drawing.Size(36, 23);
            this.RandomizeGermsButton.TabIndex = 14;
            this.RandomizeGermsButton.Text = "Set";
            this.RandomizeGermsButton.UseVisualStyleBackColor = true;
            this.RandomizeGermsButton.Click += new System.EventHandler(this.RandomizeGermsButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 369);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Germs";
            // 
            // StatusStrip
            // 
            this.StatusStrip.Location = new System.Drawing.Point(0, 582);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(872, 22);
            this.StatusStrip.TabIndex = 17;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // GermsCountLabel
            // 
            this.GermsCountLabel.AutoSize = true;
            this.GermsCountLabel.Location = new System.Drawing.Point(0, 587);
            this.GermsCountLabel.Name = "GermsCountLabel";
            this.GermsCountLabel.Size = new System.Drawing.Size(49, 13);
            this.GermsCountLabel.TabIndex = 18;
            this.GermsCountLabel.Text = "Germs: 0";
            // 
            // WorkingStatusLabel
            // 
            this.WorkingStatusLabel.AutoSize = true;
            this.WorkingStatusLabel.Location = new System.Drawing.Point(297, 587);
            this.WorkingStatusLabel.Name = "WorkingStatusLabel";
            this.WorkingStatusLabel.Size = new System.Drawing.Size(24, 13);
            this.WorkingStatusLabel.TabIndex = 19;
            this.WorkingStatusLabel.Text = "Idle";
            // 
            // OperationTabControl
            // 
            this.OperationTabControl.Controls.Add(this.GrainGrowthTabPage);
            this.OperationTabControl.Controls.Add(this.DRXTabPage);
            this.OperationTabControl.Controls.Add(this.DualPhaseTabPage);
            this.OperationTabControl.Location = new System.Drawing.Point(3, 12);
            this.OperationTabControl.Name = "OperationTabControl";
            this.OperationTabControl.SelectedIndex = 0;
            this.OperationTabControl.Size = new System.Drawing.Size(290, 567);
            this.OperationTabControl.TabIndex = 20;
            // 
            // GrainGrowthTabPage
            // 
            this.GrainGrowthTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.GrainGrowthTabPage.Controls.Add(this.InclusionGroupBox);
            this.GrainGrowthTabPage.Controls.Add(this.Boundaries);
            this.GrainGrowthTabPage.Controls.Add(this.StartGrainGrowthButton);
            this.GrainGrowthTabPage.Controls.Add(this.AlgorithmCombo);
            this.GrainGrowthTabPage.Controls.Add(this.label1);
            this.GrainGrowthTabPage.Controls.Add(this.label4);
            this.GrainGrowthTabPage.Controls.Add(this.label2);
            this.GrainGrowthTabPage.Controls.Add(this.RandomizeGermsButton);
            this.GrainGrowthTabPage.Controls.Add(this.GermsRandomizingCombo);
            this.GrainGrowthTabPage.Controls.Add(this.RevertCellsButton);
            this.GrainGrowthTabPage.Controls.Add(this.ResetButton);
            this.GrainGrowthTabPage.Controls.Add(this.GermsListView);
            this.GrainGrowthTabPage.Controls.Add(this.GermRandomizeSettingsTabControl);
            this.GrainGrowthTabPage.Location = new System.Drawing.Point(4, 22);
            this.GrainGrowthTabPage.Name = "GrainGrowthTabPage";
            this.GrainGrowthTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.GrainGrowthTabPage.Size = new System.Drawing.Size(282, 541);
            this.GrainGrowthTabPage.TabIndex = 0;
            this.GrainGrowthTabPage.Text = "Grain growth";
            // 
            // InclusionGroupBox
            // 
            this.InclusionGroupBox.Controls.Add(this.label6);
            this.InclusionGroupBox.Controls.Add(this.label3);
            this.InclusionGroupBox.Controls.Add(this.label7);
            this.InclusionGroupBox.Controls.Add(this.InclusionTypeComboBox);
            this.InclusionGroupBox.Controls.Add(this.InclusionAmountNUD);
            this.InclusionGroupBox.Controls.Add(this.InclusionSizeNUD);
            this.InclusionGroupBox.Location = new System.Drawing.Point(3, 234);
            this.InclusionGroupBox.Name = "InclusionGroupBox";
            this.InclusionGroupBox.Size = new System.Drawing.Size(272, 100);
            this.InclusionGroupBox.TabIndex = 17;
            this.InclusionGroupBox.TabStop = false;
            this.InclusionGroupBox.Text = "Inclusion";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(111, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Amount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Inclusion";
            // 
            // InclusionTypeComboBox
            // 
            this.InclusionTypeComboBox.FormattingEnabled = true;
            this.InclusionTypeComboBox.Items.AddRange(new object[] {
            "none",
            "circular before",
            "circular after",
            "square before",
            "square after"});
            this.InclusionTypeComboBox.Location = new System.Drawing.Point(6, 34);
            this.InclusionTypeComboBox.Name = "InclusionTypeComboBox";
            this.InclusionTypeComboBox.Size = new System.Drawing.Size(102, 21);
            this.InclusionTypeComboBox.TabIndex = 2;
            // 
            // InclusionAmountNUD
            // 
            this.InclusionAmountNUD.Location = new System.Drawing.Point(152, 34);
            this.InclusionAmountNUD.Name = "InclusionAmountNUD";
            this.InclusionAmountNUD.Size = new System.Drawing.Size(32, 20);
            this.InclusionAmountNUD.TabIndex = 1;
            // 
            // InclusionSizeNUD
            // 
            this.InclusionSizeNUD.Location = new System.Drawing.Point(114, 34);
            this.InclusionSizeNUD.Name = "InclusionSizeNUD";
            this.InclusionSizeNUD.Size = new System.Drawing.Size(32, 20);
            this.InclusionSizeNUD.TabIndex = 0;
            // 
            // GermRandomizeSettingsTabControl
            // 
            this.GermRandomizeSettingsTabControl.Controls.Add(this.BlankTab);
            this.GermRandomizeSettingsTabControl.Controls.Add(this.RandomWithRadiusTab);
            this.GermRandomizeSettingsTabControl.Location = new System.Drawing.Point(3, 115);
            this.GermRandomizeSettingsTabControl.Name = "GermRandomizeSettingsTabControl";
            this.GermRandomizeSettingsTabControl.SelectedIndex = 0;
            this.GermRandomizeSettingsTabControl.Size = new System.Drawing.Size(276, 79);
            this.GermRandomizeSettingsTabControl.TabIndex = 16;
            // 
            // BlankTab
            // 
            this.BlankTab.BackColor = System.Drawing.SystemColors.Control;
            this.BlankTab.Location = new System.Drawing.Point(4, 22);
            this.BlankTab.Name = "BlankTab";
            this.BlankTab.Padding = new System.Windows.Forms.Padding(3);
            this.BlankTab.Size = new System.Drawing.Size(268, 53);
            this.BlankTab.TabIndex = 0;
            this.BlankTab.Text = "Blank";
            // 
            // RandomWithRadiusTab
            // 
            this.RandomWithRadiusTab.BackColor = System.Drawing.SystemColors.Control;
            this.RandomWithRadiusTab.Controls.Add(this.label5);
            this.RandomWithRadiusTab.Controls.Add(this.RandomGermsPositionMinimumRadiusNUD);
            this.RandomWithRadiusTab.Location = new System.Drawing.Point(4, 22);
            this.RandomWithRadiusTab.Name = "RandomWithRadiusTab";
            this.RandomWithRadiusTab.Padding = new System.Windows.Forms.Padding(3);
            this.RandomWithRadiusTab.Size = new System.Drawing.Size(268, 53);
            this.RandomWithRadiusTab.TabIndex = 1;
            this.RandomWithRadiusTab.Text = "Random with radius";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Radius";
            // 
            // RandomGermsPositionMinimumRadiusNUD
            // 
            this.RandomGermsPositionMinimumRadiusNUD.Location = new System.Drawing.Point(90, 3);
            this.RandomGermsPositionMinimumRadiusNUD.Name = "RandomGermsPositionMinimumRadiusNUD";
            this.RandomGermsPositionMinimumRadiusNUD.Size = new System.Drawing.Size(41, 20);
            this.RandomGermsPositionMinimumRadiusNUD.TabIndex = 0;
            // 
            // DRXTabPage
            // 
            this.DRXTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.DRXTabPage.Controls.Add(this.StartDRXButton);
            this.DRXTabPage.Location = new System.Drawing.Point(4, 22);
            this.DRXTabPage.Name = "DRXTabPage";
            this.DRXTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.DRXTabPage.Size = new System.Drawing.Size(282, 541);
            this.DRXTabPage.TabIndex = 1;
            this.DRXTabPage.Text = "Dynamic recrystallization";
            // 
            // StartDRXButton
            // 
            this.StartDRXButton.Location = new System.Drawing.Point(6, 6);
            this.StartDRXButton.Name = "StartDRXButton";
            this.StartDRXButton.Size = new System.Drawing.Size(93, 41);
            this.StartDRXButton.TabIndex = 0;
            this.StartDRXButton.Text = "DRX Step";
            this.StartDRXButton.UseVisualStyleBackColor = true;
            this.StartDRXButton.Click += new System.EventHandler(this.StartDRXButton_Click);
            // 
            // DualPhaseTabPage
            // 
            this.DualPhaseTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.DualPhaseTabPage.Controls.Add(this.DualPhaseKeepOriginalColorCheckBox);
            this.DualPhaseTabPage.Controls.Add(this.RunDualPhaseSecondGrainGrowthButton);
            this.DualPhaseTabPage.Controls.Add(this.SelectingMartensiteGrainCheckBox);
            this.DualPhaseTabPage.Location = new System.Drawing.Point(4, 22);
            this.DualPhaseTabPage.Name = "DualPhaseTabPage";
            this.DualPhaseTabPage.Size = new System.Drawing.Size(282, 541);
            this.DualPhaseTabPage.TabIndex = 2;
            this.DualPhaseTabPage.Text = "Dual Phase";
            // 
            // RunDualPhaseSecondGrainGrowthButton
            // 
            this.RunDualPhaseSecondGrainGrowthButton.Location = new System.Drawing.Point(5, 48);
            this.RunDualPhaseSecondGrainGrowthButton.Name = "RunDualPhaseSecondGrainGrowthButton";
            this.RunDualPhaseSecondGrainGrowthButton.Size = new System.Drawing.Size(143, 23);
            this.RunDualPhaseSecondGrainGrowthButton.TabIndex = 14;
            this.RunDualPhaseSecondGrainGrowthButton.Text = "Run second grain growth";
            this.RunDualPhaseSecondGrainGrowthButton.UseVisualStyleBackColor = true;
            this.RunDualPhaseSecondGrainGrowthButton.Click += new System.EventHandler(this.RunDualPhaseSecondGrainGrowthButton_Click);
            // 
            // SelectingMartensiteGrainCheckBox
            // 
            this.SelectingMartensiteGrainCheckBox.AutoSize = true;
            this.SelectingMartensiteGrainCheckBox.Location = new System.Drawing.Point(5, 3);
            this.SelectingMartensiteGrainCheckBox.Name = "SelectingMartensiteGrainCheckBox";
            this.SelectingMartensiteGrainCheckBox.Size = new System.Drawing.Size(152, 17);
            this.SelectingMartensiteGrainCheckBox.TabIndex = 0;
            this.SelectingMartensiteGrainCheckBox.Text = "Selecting martensite grains";
            this.SelectingMartensiteGrainCheckBox.UseVisualStyleBackColor = true;
            // 
            // DualPhaseKeepOriginalColorCheckBox
            // 
            this.DualPhaseKeepOriginalColorCheckBox.AutoSize = true;
            this.DualPhaseKeepOriginalColorCheckBox.Location = new System.Drawing.Point(5, 25);
            this.DualPhaseKeepOriginalColorCheckBox.Name = "DualPhaseKeepOriginalColorCheckBox";
            this.DualPhaseKeepOriginalColorCheckBox.Size = new System.Drawing.Size(113, 17);
            this.DualPhaseKeepOriginalColorCheckBox.TabIndex = 15;
            this.DualPhaseKeepOriginalColorCheckBox.Text = "Keep original color";
            this.DualPhaseKeepOriginalColorCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 604);
            this.Controls.Add(this.OperationTabControl);
            this.Controls.Add(this.WorkingStatusLabel);
            this.Controls.Add(this.GermsCountLabel);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.DrawingPanel);
            this.Name = "MainForm";
            this.Text = "Grain Growth";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Boundaries.ResumeLayout(false);
            this.Boundaries.PerformLayout();
            this.OperationTabControl.ResumeLayout(false);
            this.GrainGrowthTabPage.ResumeLayout(false);
            this.GrainGrowthTabPage.PerformLayout();
            this.InclusionGroupBox.ResumeLayout(false);
            this.InclusionGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InclusionAmountNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InclusionSizeNUD)).EndInit();
            this.GermRandomizeSettingsTabControl.ResumeLayout(false);
            this.RandomWithRadiusTab.ResumeLayout(false);
            this.RandomWithRadiusTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RandomGermsPositionMinimumRadiusNUD)).EndInit();
            this.DRXTabPage.ResumeLayout(false);
            this.DualPhaseTabPage.ResumeLayout(false);
            this.DualPhaseTabPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel DrawingPanel;
		private System.Windows.Forms.Button StartGrainGrowthButton;
		private System.Windows.Forms.GroupBox Boundaries;
		private System.Windows.Forms.RadioButton AbsorbingRadio;
		private System.Windows.Forms.RadioButton PeriodicRadio;
		private System.Windows.Forms.ComboBox AlgorithmCombo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox GermsRandomizingCombo;
		private System.Windows.Forms.ListView GermsListView;
		private System.Windows.Forms.Button ResetButton;
		private System.Windows.Forms.Button RevertCellsButton;
		private System.Windows.Forms.Button RandomizeGermsButton;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.StatusStrip StatusStrip;
		private System.Windows.Forms.Label GermsCountLabel;
		private System.Windows.Forms.Label WorkingStatusLabel;
		private System.Windows.Forms.TabControl OperationTabControl;
		private System.Windows.Forms.TabPage GrainGrowthTabPage;
		private System.Windows.Forms.TabPage DRXTabPage;
		private System.Windows.Forms.Button StartDRXButton;
		private System.Windows.Forms.TabControl GermRandomizeSettingsTabControl;
		private System.Windows.Forms.TabPage BlankTab;
		private System.Windows.Forms.TabPage RandomWithRadiusTab;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown RandomGermsPositionMinimumRadiusNUD;
        private System.Windows.Forms.GroupBox InclusionGroupBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox InclusionTypeComboBox;
        private System.Windows.Forms.NumericUpDown InclusionAmountNUD;
        private System.Windows.Forms.NumericUpDown InclusionSizeNUD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage DualPhaseTabPage;
        private System.Windows.Forms.Button RunDualPhaseSecondGrainGrowthButton;
        private System.Windows.Forms.CheckBox SelectingMartensiteGrainCheckBox;
        private System.Windows.Forms.CheckBox DualPhaseKeepOriginalColorCheckBox;
	}
}

