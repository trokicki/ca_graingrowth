﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAGrainGrowthLibrary;

namespace CA_GrainGrowth
{
    public partial class MainForm : Form
    {
        //TODO: bring it to strings resx
        private readonly int[] _cellPaddings = { 1, 1 };
        private readonly Cell[,] _cells = new Cell[50, 50];
        private readonly CellsNeighbourhoodChecker _cellsNeighbourhoodChecker =
            new CellsNeighbourhoodChecker(CellsNeighbourhoodChecker.NeighbourhoodType.Absorbing);
        private readonly ColorRandomizer _colorRandomizer = new ColorRandomizer();
        private readonly SimulationStatus _simulationStatus = SimulationStatus.Instance;
        private readonly Random _random = new Random();
        private const string StartGrainGrowthButtonText = "Start grain growth";
        private Size _cellSize = new Size(14, 14);
        private List<Germ> _germs = new List<Germ>();
        private Func<Cell[,], int, int, int> _neighbourhoodFunc;
        private bool _toggleCellTypeOnClick;

        #region automatic generating germs

        private void GenerateRandomGerms(int germsAmount, int radius = 0)
        {
            var germsCreated = 0;
            var iterations = 0;
            var maxIterations = germsAmount * 10;
            while (germsCreated < germsAmount - 1 && iterations < maxIterations)
            {
                var row = _random.Next(_cells.GetUpperBound(0));
                var column = _random.Next(_cells.GetUpperBound(1));
                if (_cells[row, column].GermID == 0 && cellCanBeGerm(row, column, radius))
                {
                    ToggleCellState(_cells[row, column]);
                    germsCreated++;
                }
                iterations++;
            }
        }

        private void placeGermsRegularly(int germsAmount)
        {
            var rowStep = _cells.GetLength(0) / (int)Math.Ceiling(Math.Sqrt(germsAmount));
            var columnStep = _cells.GetLength(1) / (int)Math.Ceiling(Math.Sqrt(germsAmount));
            for (var row = 0; row < _cells.GetLength(0); row += rowStep)
            {
                for (var column = 0; column < _cells.GetLength(1); column += columnStep)
                {
                    if (_cells[row, column].GermID == 0)
                    {
                        ToggleCellState(_cells[row, column]);
                    }
                }
            }
        }

        private bool cellCanBeGerm(int cellRow, int cellColumn, int radius)
        {
            if (radius == 0)
                return true;
            for (var row = Math.Max(0, cellRow - radius);
                row < Math.Min(_cells.GetUpperBound(0), cellRow + radius);
                row++)
            {
                for (var column = Math.Max(0, cellColumn - radius);
                    column < Math.Min(_cells.GetUpperBound(1), cellColumn + radius);
                    column++)
                {
                    if (_cells[row, column].GermID != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        #endregion

        #region DRX

        private readonly DRXCellsNeighbourhoodChecker drxCellsNeighbourhoodChecker =
            new DRXCellsNeighbourhoodChecker(DRXCellsNeighbourhoodChecker.NeighbourhoodType.Absorbing);

        private double aParam;
        private double bParam;
        private double bordersToNonBordersRatio = 5.0 / 2;
        private double criticalDensity;
        private int dislocationsPerPackage = 4;

        private int packagesAmount;
        private int packagesToBorders;
        private int packagesToGrains;
        private double time;

        private void initDRXparams()
        {
            aParam = 8.6711 * Math.Pow(10, 13);
            bParam = 9.41;

            criticalDensity = (aParam / bParam) + (1 - (aParam / bParam)) * Math.Exp(bParam * -0.65) / 10000000000;
            //Int32.Parse(CriticalRhoTextBox.Text);
            criticalDensity = 10;
            packagesAmount = 100; // (int)PackagesAmountNUD.Value;
            packagesToBorders = (int)(bordersToNonBordersRatio * packagesAmount / (bordersToNonBordersRatio + 1.0));
            packagesToGrains = packagesAmount - packagesToBorders;
        }

        private void initialDislocationsDensity()
        {
            foreach (var cell in _cells)
            {
                cell.DislocationDensity = 0;
            }
        }

        private void StartDRXButton_Click(object sender, EventArgs e)
        {
            initDRXparams();
            time += 0.001;
            var iterations = 0;
            var maxIterations = 1;
            var hasChanges = false;
            while (!hasChanges /*iterations < maxIterations*/)
            {
                iterations++;
                if (checkNeighbourhoodBasedRecrystallization())
                {
                    hasChanges = true;
                }
                findGrainBorderCells();
                if (addPackagesToCells())
                    hasChanges = true;
                if (hasChanges)
                    DrawCells();
            }
        }

        private bool checkNeighbourhoodBasedRecrystallization()
        {
            var output = false;
            var recrystallizedCells = new List<Cell>();
            for (var row = 0; row < _cells.GetLength(0); row++)
            {
                for (var column = 0; column < _cells.GetLength(1); column++)
                {
                    var germId = drxCellsNeighbourhoodChecker.VonNeumann(_cells, row, column);
                    if (germId > 0)
                    {
                        _cells[row, column].GermID = germId;
                        _cells[row, column].DislocationDensity = 0;
                        recrystallizedCells.Add(_cells[row, column]);
                        output = true;
                    }
                }
            }
            //if(recrystallizedCells.Count > 0)
            //	Console.WriteLine("Recrystallized in this step: {0}", recrystallizedCells.Count);
            foreach (var cell in _cells)
            {
                cell.HasRecrystallized = false;
            }
            foreach (var cell in recrystallizedCells)
            {
                cell.HasRecrystallized = true;
            }
            return output;
        }

        private bool addPackagesToCells()
        {
            var random = new Random();
            var cellsRows = _cells.GetUpperBound(0);
            var cellsColumns = _cells.GetUpperBound(1);
            var output = false;
            for (var i = 0; i < packagesToGrains; i++)
            {
                if (packageToCell(random, cellsRows, cellsColumns, true))
                    output = true;
            }
            for (var i = 0; i < packagesToGrains; i++)
            {
                if (packageToCell(random, cellsRows, cellsColumns, false))
                    output = true;
            }
            return output;
        }

        private bool packageToCell(Random random, int cellsRows, int cellsColumns, bool addToBorder)
        {
            var output = false;
            var packageAdded = false;
            do
            {
                var row = random.Next(cellsRows + 1);
                var column = random.Next(cellsColumns + 1);
                if (addToBorder && _cells[row, column].IsBorder || !addToBorder)
                {
                    _cells[row, column].DislocationDensity += dislocationsPerPackage;
                    packageAdded = true;

                    #region SecondTransformationRule

                    if (_cells[row, column].DislocationDensity >= criticalDensity)
                    {
                        var newGerm = new Germ
                        {
                            Color = _colorRandomizer.RandomColor()
                        };
                        _germs.Add(newGerm);
                        _cells[row, column].DislocationDensity = 0;
                        _cells[row, column].GermID = newGerm.ID;
                        _cells[row, column].HasRecrystallized = true;
                        output = true;
                    }

                    #endregion
                }
            } while (!packageAdded);
            return output;
        }

        private void findGrainBorderCells()
        {
            //TODO: needs a lot of optimalization
            for (var row = 0; row < _cells.GetLength(0); row++)
            {
                for (var column = 0; column < _cells.GetLength(1); column++)
                {
                    if (cellIsBorder(row, column))
                        _cells[row, column].IsBorder = true;
                }
            }
        }

        private bool cellIsBorder(int row, int column)
        {
            //TODO: check remaining conditions
            if (_cells[row, column].IsBorder)
                return true;
            var germId = _cells[row, column].GermID;
            if (row < _cells.GetUpperBound(0) && column < _cells.GetUpperBound(1))
            {
                if (_cells[row, column + 1].GermID != germId ||
                    _cells[row + 1, column].GermID != germId ||
                    _cells[row + 1, column + 1].GermID != germId) ;
                return true;
            }
            if (row == _cells.GetUpperBound(0))
            {
                if (column > 0)
                {
                    if (_cells[row, column - 1].GermID != germId || _cells[row - 1, column - 1].GermID != germId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        public MainForm()
        {
            InitializeComponent();
            CalculateCellSize();
            InitializeAll();
            InclusionTypeComboBox.SelectedIndex = 0;
            GermRandomizeSettingsTabControl.Region = new Region(GermRandomizeSettingsTabControl.DisplayRectangle);
        }

        #region ControlsEvents

        private void DrawingPanel_Paint(object sender, PaintEventArgs e)
        {
            DrawCells();
        }

        private void DrawingPanel_Click(object sender, EventArgs e)
        {
            var point = DrawingPanel.PointToClient(Cursor.Position);
            var cell = GetCellByLocation(point);
            if (cell != null)
            {
                if (_toggleCellTypeOnClick)
                {
                    ToggleCellState(cell);
                }
                else if (SelectingMartensiteGrainCheckBox.Checked)
                {
                    GenerateMartensiteGrain(cell);
                }
            }

            DrawCells();
        }

        private void GenerateMartensiteGrain(Cell referenceCell)
        {
            foreach (var cell in _cells)
            {
                if (cell.GermID == referenceCell.GermID)
                {
                    cell.IsMartensite = true;
                }
            }
        }

        private void GermsRandomizingCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItemText = (sender as ComboBox).SelectedItem.ToString();
            GermRandomizeSettingsTabControl.SelectedTab = BlankTab;
            if (selectedItemText == "Click")
            {
                RandomizeGermsButton.Enabled = false;
                _toggleCellTypeOnClick = true;
            }
            else if (selectedItemText == "Random considering radius")
            {
                GermRandomizeSettingsTabControl.SelectedTab = RandomWithRadiusTab;
                RandomizeGermsButton.Enabled = true;
                _toggleCellTypeOnClick = false;
            }
            else
            {
                RandomizeGermsButton.Enabled = true;
                _toggleCellTypeOnClick = false;
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            Germ.ResetCounter();
            InitializeCellsArray();
            _germs = new List<Germ>();
            GermsListView.Items.Clear();
            DrawCells();
            WorkingStatusLabel.Text = "Idle";
            GermsCountLabel.Text = String.Format("Germs: {0}", _germs.Count());
        }

        private void RevertCellsButton_Click(object sender, EventArgs e)
        {
            RevertCellsArray();
            DrawCells();
            WorkingStatusLabel.Text = "Idle";
        }

        private void RandomizeGermsButton_Click(object sender, EventArgs e)
        {
            var germsToPlace = 64; // _cells.GetLength(0) * _cells.GetLength(1) / 400 + 1;
            switch (GermsRandomizingCombo.SelectedItem.ToString())
            {
                case "Random":
                    GenerateRandomGerms(germsToPlace);
                    break;
                case "Random considering radius":
                    GenerateRandomGerms(germsToPlace, (int)RandomGermsPositionMinimumRadiusNUD.Value);
                    break;
                case "Regular":
                    placeGermsRegularly(germsToPlace);
                    break;
            }
            DrawCells();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (!_simulationStatus.Working)
            {
                StartWorking();
            }
            else
            {
                PauseWorking();
            }
        }

        private void AlgorithmCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (AlgorithmCombo.SelectedItem.ToString())
            {
                case "Von Neumann":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.VonNeumann;
                    break;
                case "Moore":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.Moore;
                    break;
                case "Pentagonal left":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.PentagonalLeft;
                    break;
                case "Pentagonal right":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.PentagonalRight;
                    break;
                case "Pentagonal top":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.PentagonalTop;
                    break;
                case "Pentagonal bottom":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.PentagonalBottom;
                    break;
                case "Pentagonal random":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.PentagonalRandom;
                    break;
                case "Hexagonal left":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.HexagonalLeft;
                    break;
                case "Hexagonal right":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.HexagonalRight;
                    break;
                case "Hexagonal random":
                    _neighbourhoodFunc = _cellsNeighbourhoodChecker.HexagonalRandom;
                    break;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _simulationStatus.Working = false;
        }

        #endregion

        private void CalculateCellSize()
        {
            var cellWidth = DrawingPanel.Width / _cells.GetLength(0);
            var cellHeight = DrawingPanel.Height / _cells.GetLength(1);
            _cellSize = new Size(cellWidth, cellHeight);
        }

        private void InitializeCellsArray()
        {
            for (var row = 0; row < _cells.GetLength(0); row++)
            {
                for (var column = 0; column < _cells.GetLength(1); column++)
                {
                    _cells[row, column] = new Cell
                    {
                        Location = new[] { row * _cellSize.Width, column * _cellSize.Height }
                    };
                }
            }
        }

        private void RevertCellsArray()
        {
            for (var row = 0; row < _cells.GetLength(0); row++)
            {
                for (var column = 0; column < _cells.GetLength(1); column++)
                {
                    if (!_cells[row, column].IsSealed)
                    {
                        _cells[row, column] = new Cell
                        {
                            Location = new[] { row * _cellSize.Width, column * _cellSize.Height }
                        };
                    }
                }
            }
        }

        private void DrawCells()
        {
            foreach (var cell in _cells)
                DrawCell(cell);
        }

        private void DrawCell(Cell cell)
        {
            var germ = _germs.FirstOrDefault(g => g.ID == cell.GermID);
            var germColor = germ != null ? germ.Color : Color.Gray;

            using (var blackBrush = new SolidBrush(Color.Black))
            using (var whiteBrush = new SolidBrush(Color.White))
            using (var cellBrush = new SolidBrush(germColor))
            using (var graphics = DrawingPanel.CreateGraphics())
            {
                var cellRectangle = new Rectangle(
                    cell.Location[0] + _cellPaddings[0],
                    cell.Location[1] + _cellPaddings[1],
                    _cellSize.Width - _cellPaddings[0],
                    _cellSize.Height - _cellPaddings[1]);
                var brush = blackBrush;
                if (cell.IsInclusion)
                    brush = blackBrush;
                if (cell.IsMartensite && !DualPhaseKeepOriginalColorCheckBox.Checked)
                {
                    brush = whiteBrush;
                }
                else
                    brush = cellBrush;

                graphics.FillRectangle(brush, cellRectangle);
            }
        }

        private void StartWorking()
        {
            if (PeriodicRadio.Checked)
            {
                _cellsNeighbourhoodChecker.CellNeighbourhoodType = CellsNeighbourhoodChecker.NeighbourhoodType.Periodic;
            }
            else if (AbsorbingRadio.Checked)
            {
                _cellsNeighbourhoodChecker.CellNeighbourhoodType = CellsNeighbourhoodChecker.NeighbourhoodType.Absorbing;
            }

            var selectedInclusionType = InclusionTypeComboBox.SelectedItem.ToString();
            var inclusionsAmount = (int)InclusionAmountNUD.Value;
            if (selectedInclusionType.Contains("before"))
            {
                SetInclusions(
                    inclusionsAmount,
                    ResolveInclusionType(selectedInclusionType));
            }

            _simulationStatus.Working = true;
            WorkingStatusLabel.Text = "Working...";
            RunWithInterval(Step);
            StartGrainGrowthButton.Text = "Pause";
        }

        private InclusionType ResolveInclusionType(string inclusionDescription)
        {
            return inclusionDescription.Contains("circular")
                ? InclusionType.Circle
                : InclusionType.Square;
        }

        private void PauseWorking()
        {
            _simulationStatus.Working = false;
            StartGrainGrowthButton.Text = StartGrainGrowthButtonText;
            WorkingStatusLabel.Text = String.Format("Paused.");
        }

        private async void RunWithInterval(Action action)
        {
            while (_simulationStatus.Working)
            {
                await Task.Run(() => action.Invoke());
            }
            //WorkingStatusLabel.Text = String.Format("Done. Elapsed time: {0}", stopwatch.Elapsed);
            //TODO: raise some custom event
            WorkingStatusLabel.Text = String.Format("Done.");
            StartGrainGrowthButton.Text = StartGrainGrowthButtonText;
        }

        private void Step()
        {
            var cellsToChange = new ConcurrentDictionary<Cell, int>();
            Parallel.For(0, _cells.GetLength(0), row =>
            {
                for (var column = 0; column < _cells.GetLength(1); column++)
                {

                    if (_cells[row, column].GermID != 0)
                        continue;

                    var newCellGermId = _neighbourhoodFunc(_cells, row, column);

                    //temp
                    var isMartensite = false;
                    foreach (var cell in _cells)
                    {
                        if (cell.GermID == newCellGermId && cell.IsMartensite)
                        {
                            isMartensite = true;
                            break;
                        }
                    }
                    if (isMartensite)
                        continue;
                    //-

                    if (newCellGermId > 0)
                    {
                        cellsToChange.TryAdd(_cells[row, column], newCellGermId);
                    }
                }
            });
            Parallel.ForEach(cellsToChange, pair => { pair.Key.GermID = pair.Value; });
            DrawCells();
            if (cellsToChange.Any())
                return;

            _simulationStatus.Working = false;
            _simulationStatus.Complete = true;
        }

        private Cell GetCellByLocation(Point location)
        {
            var column = location.X / _cellSize.Width;
            var row = location.Y / _cellSize.Height;
            if (column >= _cells.GetLength(0) || row >= _cells.GetLength(1))
                return null;
            return _cells[column, row];
        }

        private void ToggleCellState(Cell cell)
        {
            if (cell.GermID != 0)
                return;

            var newGerm = new Germ { Color = _colorRandomizer.RandomColor() };
            _germs.Add(newGerm);
            cell.GermID = newGerm.ID;
            cell.IsSealed = true;

            //TODO: hack
            GermsListView.Items.Add(new ListViewItem(new String('-', 32))
            {
                BackColor = newGerm.Color,
                ForeColor = newGerm.Color
            });
            GermsListView.Items.Add(new ListViewItem(String.Format("Germ #{0}", newGerm.ID)));
            GermsCountLabel.Text = String.Format("Germs: {0}", _germs.Count);
        }

        private void SetInclusionAt(NewInclusionParams inclusionParams, InclusionType inclusionType)
        {
            switch (inclusionType)
            {
                case InclusionType.Square:
                    SetSquareInclusionAt(inclusionParams);
                    break;
                case InclusionType.Circle:
                    SetCircularInclusionAt(inclusionParams);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("inclusionType");
            }
        }

        private void SetCircularInclusionAt(NewInclusionParams inclusionParams)
        {
            throw new NotImplementedException("Circular inclusion is not yet supported");
        }

        private void SetSquareInclusionAt(NewInclusionParams inclusionParams)
        {
            for (var i = inclusionParams.Row; i < inclusionParams.Row + inclusionParams.Size; i++)
            {
                for (var j = inclusionParams.Column; j < inclusionParams.Column + inclusionParams.Size; j++)
                {
                    var cell = _cells[i, j];
                    cell.IsInclusion = true;
                    cell.IsSealed = true;
                }
            }
        }

        private class NewInclusionParams
        {
            public int Row { get; set; }
            public int Column { get; set; }
            public int Size { get; set; }
        }

        private void InitializeAll()
        {
            InitializeCellsArray();
            _germs = new List<Germ>();
            GermsListView.Items.Clear();
            AlgorithmCombo.SelectedIndex = 0;
            GermsRandomizingCombo.SelectedIndex = 1;
            WorkingStatusLabel.Text = "Idle";
            RandomGermsPositionMinimumRadiusNUD.Value = _cells.GetLength(0) / 4;
            StartGrainGrowthButton.Text = StartGrainGrowthButtonText;
        }

        private void SetInclusions(int inclusionsAmount, InclusionType inclusionType, bool borderOnly = false)
        {
            var inclusionSize = (int)InclusionSizeNUD.Value;
            var inclusionsSet = 0;
            var trials = 0;
            const int maxTrials = 100;
            while (inclusionsSet < inclusionsAmount && trials < maxTrials)
            {
                int row, column;

                if (borderOnly)
                {
                    Tuple<int, int> newInclusionRowAndColumn = GetRandomBorderCell();
                    row = newInclusionRowAndColumn.Item1;
                    column = newInclusionRowAndColumn.Item2;
                }
                else
                {
                    row = _random.Next(_cells.GetUpperBound(0) - inclusionSize);
                    column = _random.Next(_cells.GetUpperBound(1) - inclusionSize);
                }

                if (_cells[row, column].GermID == 0 && !_cells[row, column].IsInclusion)
                {
                    SetInclusionAt(new NewInclusionParams { Row = row, Column = column, Size = inclusionSize }, inclusionType);
                    inclusionsSet++;
                }
                trials++;
            }
        }

        private Tuple<int, int> GetRandomBorderCell()
        {
            for (var i = 0; i < 100000; i++)
            {
                var row = _random.Next(_cells.GetUpperBound(0));
                var column = _random.Next(_cells.GetUpperBound(1));
                var cell = _cells[row, column];
                if (cell.IsInclusion || cell.IsMartensite || cell.GermID == 0)
                    continue;
                try
                {
                    if (
                        _cells[row - 1, column - 1].GermID != cell.GermID ||
                        _cells[row - 1, column].GermID != cell.GermID ||
                        _cells[row - 1, column + 1].GermID != cell.GermID ||
                        _cells[row, column - 1].GermID != cell.GermID ||
                        _cells[row, column + 1].GermID != cell.GermID ||
                        _cells[row + 1, column - 1].GermID != cell.GermID ||
                        _cells[row + 1, column].GermID != cell.GermID ||
                        _cells[row + 1, column + 1].GermID != cell.GermID
                        )
                    {
                        return new Tuple<int, int>(row, column);
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    //temp
                }
            }
            throw new Exception("No place for new inclusion was found.");
        }

        private enum InclusionType
        {
            Square,
            Circle
        }

        private void RunDualPhaseSecondGrainGrowthButton_Click(object sender, EventArgs e)
        {
            foreach (var cell in _cells)
            {
                if (!cell.IsMartensite)
                    cell.GermID = 0;
            }
            GenerateRandomGerms(germsAmount: 25);
            DrawCells();
            Thread.Sleep(2500);
            StartWorking();
        }

    }

    internal sealed class SimulationStatus
    {
        private static readonly SimulationStatus instance = new SimulationStatus();

        private SimulationStatus()
        {
            Working = false;
            Complete = false;
        }

        public bool Working { get; set; }
        public bool Complete { get; set; }

        public static SimulationStatus Instance
        {
            get { return instance; }
        }
    }
}